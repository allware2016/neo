//
//  Portfolio.swift
//  NOE
//
//  Created by Abdallah Nehme on 2/29/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import Foundation
struct Portfolio: Codable {
    let id: String
    let name: String?
    let created_at: String
    let balance: Int?
    let total_earnings: Int?
    let latest_daily_earning: Int?
    let modified_risk_score: Int?
    let investment_type: String?
    
    enum CodeingKeys: String , CodingKey {
        case id
        case name
        case created_at
        case balance
        case total_earnings
        case latest_daily_earning
        case modified_risk_score
        case investment_type
    }
}
extension Portfolio{
    static func save(data: [Portfolio]){
        if let data = try? JSONEncoder().encode(data),
            let string = String(data: data, encoding: .utf8){
            var key = UserDefault<String?>("portfolioData", defaultValue: string)
            key.wrappedValue = string
        }
    }
    static func get()-> [Portfolio]? {
        let key = UserDefault<String?>("portfolioData", defaultValue: nil)
        if let data = key.wrappedValue?.data(using: .utf8),
            let objectData = try? JSONDecoder().decode([Portfolio].self, from: data){
            return objectData
        }
        return nil
    }
}
