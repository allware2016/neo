//
//  ChartTableViewCell.swift
//  NOE
//
//  Created by Abdallah Nehme on 3/1/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import UIKit
import Charts

class ChartTableViewCell: UITableViewCell {
    @IBOutlet weak var chartView: LineChartView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setChartData(_ data: LineChartData){
        data.setValueFont(.systemFont(ofSize: 7, weight: .light))
        chartView.data = data
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
