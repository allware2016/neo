//
//  HistoricalData.swift
//  NOE
//
//  Created by Abdallah Nehme on 2/29/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import Foundation

struct QuarterlyDeposits: Codable {
    let quarterly_deposits: [HistoricalData]
    enum CodeingKeys: String , CodingKey {
        case quartely_deposits
    }
}
struct HistoricalData:Codable {
    let date: String
    let date_js: String
    let smartWealthValue: Int
    let benchmarkValue: Int
    
    enum CodeingKeys: String , CodingKey {
        case date
        case date_js
        case smartWealthValue
        case benchmarkValue
    }
}
extension QuarterlyDeposits{
     func save(){
        if let data = try? JSONEncoder().encode(self),
            let string = String(data: data, encoding: .utf8){
            var key = UserDefault<String?>("historicalData", defaultValue: string)
            key.wrappedValue = string
        }
    }
    static func get()-> QuarterlyDeposits? {
        let key = UserDefault<String?>("historicalData", defaultValue: nil)
        if let data = key.wrappedValue?.data(using: .utf8),
         let objectData = try? JSONDecoder().decode(QuarterlyDeposits.self, from: data){
            return objectData
        }
        return nil
    }
}
