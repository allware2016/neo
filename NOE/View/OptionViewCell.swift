//
//  OptionViewCell.swift
//  NOE
//
//  Created by Abdallah Nehme on 3/1/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import UIKit
import FSPagerView

class OptionViewCell: FSPagerViewCell {

    @IBOutlet weak var placeHolderView: UIView!
    @IBOutlet weak var optionNameLabel: UILabel!
    @IBOutlet weak var optionNumberLabel: UILabel!
    @IBOutlet weak var optionDescLabel: UILabel!
    @IBOutlet weak var optionBalanceLabel: UILabel!
    @IBOutlet weak var optionSelectedLabel: UILabel!
    @IBOutlet weak var selectedImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.applyDefaultStyle()
    }
    
    func applySelectedStyle(){
        let blueColor = UIColor(named: "AppBlue") ?? .blue
        //self.placeHolderView.backgroundColor = UIColor(named: "AppBlue")
        self.placeHolderView.setGradientBackground([blueColor,.white,blueColor], opacity: 1, direction: .center)
        self.setTextColor(.white)
        self.selectedImage.isHidden = false
    }
    func applyDefaultStyle(){
        self.placeHolderView.backgroundColor = .white
        self.setTextColor(.black)
        self.selectedImage.isHidden = true
    }
    private func setTextColor(_ color: UIColor){
        self.optionBalanceLabel.textColor = color
        self.optionDescLabel.textColor = color
        self.optionNameLabel.textColor = color
        self.optionNumberLabel.textColor = color
        self.optionSelectedLabel.textColor = color
    }
}
