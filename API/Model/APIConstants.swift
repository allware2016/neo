//
//  APIConstants.swift
//  NOE
//
//  Created by Abdallah Nehme on 2/29/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import Foundation
import Alamofire
public struct APIConstants{
    public static let BaseURL = "https://temp.boubyan-invest.com/"
    public static let appHeaders: HTTPHeaders = [
        "Content-type":"application/x-www-form-urlencoded"
    ]
}
