//
//  StringExtension.swift
//  NOE
//
//  Created by Abdallah Nehme on 3/1/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import Foundation
extension String {
    var toDate: Date? {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"
        dateFormater.locale = Locale(identifier: "en_US")
        if let date = dateFormater.date(from: self) {
            return date
        }
        return nil
    }
}
