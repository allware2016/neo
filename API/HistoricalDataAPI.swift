//
//  HistoricalDataAPI.swift
//  NOE
//
//  Created by Abdallah Nehme on 2/29/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import Foundation
import Alamofire
class HistoricalDataAPI{
    func get(completion: @escaping (_ response: QuarterlyDeposits?, _ error: Error?)->()){
        
        AF.request(APIConstants.BaseURL + "historical_data",
                   method: .get,
                   headers: APIConstants.appHeaders,
                   interceptor: nil).responseDecodable(of: QuarterlyDeposits.self) { response in
                    switch response.result{
                    case .success(let value):
                        completion(value,nil)
                    case .failure(let error):
                        completion(nil, error)
                    }
        }
    }
}
