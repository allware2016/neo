//
//  UIViewExtension.swift
//  NOE
//
//  Created by Abdallah Nehme on 3/1/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import UIKit
extension UIView {
    enum GradientColorDirection {
        case vertical
        case horizontal
        case center
    }
    
    func setGradientBackground(_ colors: [UIColor], opacity: Float = 1, direction: GradientColorDirection = .vertical, cornerRadius: CGFloat = 0.0) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.opacity = opacity
        gradientLayer.colors = colors.map { $0.cgColor }
        
        if case .horizontal = direction {
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
        }
        if case .center = direction {
            gradientLayer.startPoint  = CGPoint(x: bounds.width/2, y: bounds.height/2)
            gradientLayer.endPoint  = CGPoint(x: bounds.width/2, y: bounds.height/2)
        }
        gradientLayer.bounds = self.bounds
        gradientLayer.anchorPoint = CGPoint.zero
        gradientLayer.cornerRadius = cornerRadius
        self.layer.addSublayer(gradientLayer)
        gradientLayer.zPosition = -1
    }
}
