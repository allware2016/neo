//
//  HomeTableViewController.swift
//  NOE
//
//  Created by Abdallah Nehme on 2/29/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import UIKit
import ANLoader

class HomeTableViewController: UITableViewController {
    private var viewModel: HomeViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    private func setup(){
        self.viewModel = HomeViewModel()
        self.loadData()
        self.registerCells()
    }
    private func registerCells(){
        let nib = UINib(nibName: PortfolioTableViewCell.identifier, bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: PortfolioTableViewCell.identifier)
    }
    
    private func loadData(){
        if let portfolios = Portfolio.get() {
            self.viewModel.portfolios = portfolios
            self.tableView.reloadData()
        }else{
            ANLoader.showLoading()
            self.viewModel.fetch { (portfolios, error) in
                //TODO: Abdallah
                //Show error
                ANLoader.hide()
                guard  error == nil else { return }
                DispatchQueue.main.async {[weak self] in
                    guard let self = self else { return }
                    self.tableView.reloadData()
                }
            }
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.portfolios?.count ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PortfolioTableViewCell.identifier, for: indexPath) as! PortfolioTableViewCell

        cell.portfolioNameLabel?.text = self.viewModel.portfolios?[indexPath.row].name ?? ""
        cell.portfolioBalanceLabel?.text = (self.viewModel.portfolios?[indexPath.row].balance ?? 0).toCurrency
        cell.selectionStyle = .none
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let portfolio = self.viewModel.portfolios?[indexPath.row]
        self.performSegue(withIdentifier: "GoalStatusSegue", sender: portfolio)
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let goalStatusController = segue.destination as? GoalStatusViewController,
            segue.identifier == "GoalStatusSegue",
            let portfolio = sender as? Portfolio {
            goalStatusController.portfolio = portfolio
        }
    }
    

}
