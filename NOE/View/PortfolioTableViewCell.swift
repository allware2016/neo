//
//  PortfolioTableViewCell.swift
//  NOE
//
//  Created by Abdallah Nehme on 2/29/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import UIKit

class PortfolioTableViewCell: UITableViewCell {

    @IBOutlet weak var portfolioNameLabel: UILabel!
    @IBOutlet weak var portfolioBalanceLabel: UILabel!
    @IBOutlet weak var placeHolderView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.placeHolderView.layer.applyShadow(color: .gray,
                               alpha: 0.5,
                               x: 0,
                               y: 0,
                               blur: 6,
                               spread: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
