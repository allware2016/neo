//
//  DetailTableViewController.swift
//  NOE
//
//  Created by Abdallah Nehme on 2/29/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import UIKit
import AFDateHelper

class DetailTableViewController: UITableViewController {
    var portfolio: Portfolio?
    private struct Content {
        let title: String
        let detail: String
    }
    private var detailData = [Content]()
    @IBOutlet weak var navItem: UINavigationItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.registerCells()
    }
    private func setup(){
        self.navItem.title = portfolio?.name ?? ""
        self.formatData()
    }
    private func registerCells(){
        let nib = UINib(nibName: ChartTableViewCell.identifier, bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: ChartTableViewCell.identifier)
    }
    private func formatData(){
        self.detailData.append(DetailTableViewController.Content(title: "ID", detail: portfolio?.id ?? ""))
        self.detailData.append(DetailTableViewController.Content(title: "Created At", detail: portfolio?.created_at.toDate?.format("dd MMMM") ?? ""))
        self.detailData.append(DetailTableViewController.Content(title: "Risk Score", detail: "\(portfolio?.modified_risk_score ?? 0)"))
        self.detailData.append(DetailTableViewController.Content(title: "Investment Type", detail: portfolio?.investment_type?.uppercased() ?? ""))
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if section == 0 {
            count = self.detailData.count
        }else if section == 1 {
            count = 1
        }
        return count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            let rowData = self.detailData[indexPath.row]
            cell.textLabel?.text = rowData.title
            cell.detailTextLabel?.text = rowData.detail
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: ChartTableViewCell.identifier, for: indexPath) as! ChartTableViewCell
            if let portfolio = self.portfolio, 
                let date = portfolio.created_at.toDate {
                let chartData = HistoricalViewModel().loadChartData(yearFilter: date.format("yyyy"))
                cell.setChartData(chartData)
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 55
        }else {
            return 270
        }
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
}
