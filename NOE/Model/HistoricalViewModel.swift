//
//  HistoricalViewModel.swift
//  NOE
//
//  Created by Abdallah Nehme on 3/1/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import Foundation
import Charts
class HistoricalViewModel {
    var hostoricalData: QuarterlyDeposits?
    struct LineData {
        let value: Double
        let date: String
    }
    func fetch(completion: @escaping (QuarterlyDeposits?, Error?)->()) {
        HistoricalDataAPI().get { [weak self] (hostoricalData, error) in
            if  hostoricalData  != nil {
                hostoricalData?.save()
            }
            guard let self = self else { completion(nil,nil); return }
            self.hostoricalData = hostoricalData
            completion(hostoricalData, error)
        }
    }
    
    func loadChartData(yearFilter: String) -> LineChartData {
        guard let historyData = QuarterlyDeposits.get()?.quarterly_deposits else {
            let data = LineChartData(dataSets: [])
            return data
        }
        let smartWealthData = historyData.filter({$0.date.contains(yearFilter)}).map({LineData(value: Double($0.smartWealthValue), date: $0.date)})
        let benchmarkData = historyData.filter({$0.date.contains(yearFilter)}).map({LineData(value: Double($0.benchmarkValue), date: $0.date)})
        
        let smartWealthChartDataEntry = smartWealthData.enumerated().map({ChartDataEntry(x:Double($0.offset), y: $0.element.value)})
        let benchmarkChartDataEntry = benchmarkData.enumerated().map({ChartDataEntry(x:Double($0.offset), y: $0.element.value)})
        
        var dataSets = [LineChartDataSet]()
        let set1 = LineChartDataSet(entries: smartWealthChartDataEntry, label: "Smartwealth")
        set1.lineWidth = 2.5
        set1.circleRadius = 0
        set1.circleHoleRadius = 0
        set1.setColor(.blue)
        
        let set2 = LineChartDataSet(entries: benchmarkChartDataEntry, label: "Benchmark")
        set2.lineWidth = 2.5
        set2.circleRadius = 0
        set2.circleHoleRadius = 0
        set2.setColor(.black)
        dataSets.append(set1)
        dataSets.append(set2)

        return LineChartData(dataSets: dataSets)
    }
}
