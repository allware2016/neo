//
//  Option.swift
//  NOE
//
//  Created by Abdallah Nehme on 2/29/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import Foundation
struct Option: Codable {
    let id: String
    let name: String
    let short_description: String
    let risk_score: Int
    
    enum CodeingKeys: String , CodingKey {
        case id
        case name
        case short_description
        case risk_score
    }
}
extension Option{
    static func save(data: [Option]){
        if let data = try? JSONEncoder().encode(data),
            let string = String(data: data, encoding: .utf8){
            var key = UserDefault<String?>("OptionData", defaultValue: string)
            key.wrappedValue = string
        }
    }
    static func get()-> [Option]? {
        let key = UserDefault<String?>("OptionData", defaultValue: nil)
        if let data = key.wrappedValue?.data(using: .utf8),
            let objectData = try? JSONDecoder().decode([Option].self, from: data){
            return objectData
        }
        return nil
    }
}
