//
//  DateExtension.swift
//  NOE
//
//  Created by Abdallah Nehme on 3/1/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import Foundation
extension Date {
    func format(_ dateFormat: String) -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = dateFormat
        dateFormater.locale = Locale(identifier: "en_US")
        return dateFormater.string(from: self)
    }
    
}
