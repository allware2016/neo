//
//  HomeViewModel.swift
//  NOE
//
//  Created by Abdallah Nehme on 2/29/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import Foundation
class HomeViewModel {
    var portfolios: [Portfolio]?
    
    func fetch(completion: @escaping ([Portfolio]?, Error?)->()) {
        PortfoliosAPI().get { [weak self] (portfolios, error) in
            if let data = portfolios  {
                Portfolio.save(data: data)
            }
            guard let self = self else { completion(nil,nil); return }
            self.portfolios = portfolios
            completion(portfolios, error)
        }
    }
}
