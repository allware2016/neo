//
//  GoalStatusViewModel.swift
//  NOE
//
//  Created by Abdallah Nehme on 3/1/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import Foundation
class GoalStatusViewModel {
    var options: [Option]?
    
    func fetch(completion: @escaping ([Option]?, Error?)->()) {
        OptionsAPI().get { [weak self] (options, error) in
            if let data = options  {
                Option.save(data: data)
            }
            guard let self = self else { completion(nil,nil); return }
            self.options = options
            completion(options, error)
        }
    }
}
