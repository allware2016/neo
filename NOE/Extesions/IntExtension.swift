//
//  IntExtension.swift
//  NOE
//
//  Created by Abdallah Nehme on 3/1/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import Foundation
extension Int {
    var toCurrency:String{
        let numberFormatter = NumberFormatter()
        let usLocale = Locale(identifier: "en_US")
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = usLocale
        return numberFormatter.string(from: NSNumber(value: Double(self)))!
    }
}
