//
//  UITableViewCellExtension.swift
//  NOE
//
//  Created by Abdallah Nehme on 2/29/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import UIKit
extension UITableViewCell {
   static var identifier: String {
        return String(describing: Self.self)
    }
}
