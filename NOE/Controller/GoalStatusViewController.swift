//
//  GoalStatusViewController.swift
//  NOE
//
//  Created by Abdallah Nehme on 2/29/20.
//  Copyright © 2020 Abdallah Nehme. All rights reserved.
//

import UIKit
import FSPagerView
import ANLoader

class GoalStatusViewController: UIViewController {
    var portfolio: Portfolio?
    var viewModel: GoalStatusViewModel!
    
    @IBOutlet weak var pageControl: FSPageControl!{
        didSet {
            self.pageControl.contentHorizontalAlignment = .center
            self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            self.pageControl.currentPage = 0
            self.pageControl.setFillColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), for: .normal)
            self.pageControl.setFillColor(UIColor(named: "AppBlue"), for: .selected)
            
        }
    }
    @IBOutlet weak var optionsPagerView: FSPagerView!{
        didSet{
            // self.packagesPagerView.register(PackageViewCell.self, forCellWithReuseIdentifier: "cell")
            let nib = UINib(nibName: "OptionViewCell", bundle: nil)
            self.optionsPagerView.register(nib, forCellWithReuseIdentifier: "cell")
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.loadData()
    }
    private func setup(){
        self.viewModel = GoalStatusViewModel()
        self.optionsPagerView.transformer = FSPagerViewTransformer(type: .linear)
        self.optionsPagerView.itemSize = CGSize(width: 350, height: 350)
    }
    private func loadData(){
        if let options = Option.get() {
            self.viewModel.options = options
            self.optionsPagerView.reloadData()
        }else{
            ANLoader.showLoading()
            self.viewModel.fetch { (options, error) in
                //TODO: Abdallah
                //Show error
                ANLoader.hide()
                guard  error == nil else { return }
                DispatchQueue.main.async {[weak self] in
                    guard let self = self else { return }
                    self.optionsPagerView.reloadData()
                }
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailController = segue.destination as? DetailTableViewController,
            segue.identifier == "DetailSegue"{
            detailController.portfolio = portfolio
        }
    }
    
    
}
extension GoalStatusViewController: FSPagerViewDelegate, FSPagerViewDataSource{
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        self.pageControl.numberOfPages = self.viewModel.options?.count ?? 0
        self.pageControl.currentPage = 0
        return self.viewModel.options?.count ?? 0
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index) as! OptionViewCell
        if let rowData = self.viewModel.options?[index] {
            cell.optionNumberLabel.text = "Option \(index + 1)"
            cell.optionDescLabel.text = rowData.short_description
            cell.optionNameLabel.text = rowData.name
            cell.optionBalanceLabel.text = (portfolio?.balance ?? 0).toCurrency
            cell.applyDefaultStyle()
        }
        return cell
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
        if let cell = pagerView.cellForItem(at: targetIndex) as? OptionViewCell {
            cell.applySelectedStyle()
        }
        
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControl.currentPage = pagerView.currentIndex
    }
}
